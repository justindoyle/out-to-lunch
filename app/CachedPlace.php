<?php

namespace OutToLunch;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CachedPlace extends Model
{
    const MAX_CACHE_AGE = 60 * 24 * 7; // 1 week

    protected $casts = [
        'cached_place' => 'array'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'last_cached_at',
    ];



    public function getCacheAgeAttribute()
    {
        $now = Carbon::now();

        if($this->attributes['last_cached_at'] === null) {
            return null;
        }

        return $now->diffInMinutes($this->attributes['last_cached_at']);
    }

    public function recommendations() {
        return $this->hasMany(Recommendation::class, 'id', 'cahced_place_id');
    }
}
