<?php
namespace OutToLunch\Classes\Recommendations\Enums;

use OutToLunch\Classes\Enum;

abstract class Atmosphere extends Enum
{
    const Date = 'date';
    const FamilyFriendly = 'family_friendly';
    const Friends = 'friends';
}