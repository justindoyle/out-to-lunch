<?php
namespace OutToLunch\Classes\Recommendations;

use Carbon\Carbon;
use Location\Coordinate;
use Location\Distance\Vincenty;
use OutToLunch\Classes\GooglePlacesHttp;
use OutToLunch\Recommendation;

/**
 * Class RecommendationBuilder
 *
 * A builder class designed to gather user input and
 * return a list of recommendations based on nearby restaurants from
 * Google's Places API.
 *
 * @package OutToLunch\Classes\Recommendations
 */
class RecommendationBuilder
{
    const MAX_RATING = 5.0;
    const MAX_PRICE_LEVEL = 4.0;
    const MAX_DRIVING_DISTANCE = 10000;
    const MAX_WALKING_DISTANCE = 800;

    private $placesHttp;
    private $latitude;
    private $longitude;
    private $atmosphere;
    private $cuisine;
    private $distance;
    private $price;
    private $quality;
    private $speed;
    private $isOpenNow = false;
    private $isWalking = true;
    private $openAfter;
    private $openUntil;
    private $city = '';


    /**
     * RecommendationBuilder constructor.
     * @param GooglePlacesHttp The http client for accessing Google Places API
     * @param $myLatitude The user's current latitude
     * @param $myLongitude The user's current longitude
     * @param $myCity The user's city, state, country
     */
    public function __construct($placesHttpApi, $myLatitude, $myLongitude, $myCity = '')
    {
        $this->placesHttp = $placesHttpApi;

        $this->latitude = $myLatitude;
        $this->longitude = $myLongitude;
        $this->city = $myCity;
    }

    /**
     * Set the desired atmosphere
     *
     * @param $atmosphere The atmosphere
     * @return RecommendationBuilder $this
     */
    public function atmosphere($atmosphere)
    {
        $this->atmosphere = $atmosphere;

        return $this;
    }

    /**
     * Set the desired cuisine
     * @param $cuisine The cuisine
     * @return RecommendationBuilder $this
     */
    public function cuisine($cuisine)
    {
        $this->cuisine = $cuisine;

        return $this;
    }

    /**
     * The importance of proximity to your location
     *
     * @throws \DomainException
     * @param float $distance Weight of importance
     * @return RecommendationBuilder $this
     */
    public function distance($distance)
    {
        if(is_numeric($distance)) {
            $this->distance = $distance;
        }

        return $this;
    }

    /**
     * The importance of a low price
     *
     * @throws \DomainException
     * @param float $price Weight of importance
     * @return RecommendationBuilder $this
     */
    public function price($price)
    {
        if(is_numeric($price)) {
            $this->price = $price;
        }

        return $this;
    }

    /**
     * The importance of a high quality
     *
     * @throws \DomainException
     * @param float $quality Weight of importance
     * @return RecommendationBuilder $this
     */
    public function quality($quality)
    {
        if(is_numeric($quality)) {
            $this->quality = $quality;
        }

        return $this;
    }

    /**
     * The importance of a high speed of service
     *
     * @throws \DomainException
     * @param float $speed Weight of importance from 0-1
     * @return RecommendationBuilder $this
     */
    public function speed($speed)
    {
        if(is_numeric($speed)) {
            $this->speed = (float) $speed;

            if($this->speed > 1 || $this->speed < 0) {
                throw new \DomainException("Speed weight must be float between 0 and 1");
            }
        }

        return $this;
    }


    /**
     * Require that establishment is currently open
     *
     * @param boolean $isOpen
     * @return RecommendationBuilder $this
     */
    public function isOpenNow($isOpen)
    {
        $this->isOpenNow = $isOpen;

        return $this;
    }

    public function amDriving()
    {
        $this->isWalking = false;

        return $this;
    }

    public function amWalking()
    {
        $this->isWalking = true;

        return $this;
    }


    /**
     * Require that the establishment is open during these hours.
     *
     * @param Carbon $start Date to make sure is open after
     * @param Carbon $end Date to make sure is open until
     * @return RecommendationBuilder $this
     */
    // TODO This feature is not yet implemented

//    public function isOpenBetween(Carbon $start, Carbon $end)
//    {
//        $this->openAfter = $start;
//        $this->openUntil = $end;
//
//        return $this;
//    }

    /**
     * Build a list of recommendations
     * @return RecommendationList
     */
    public function get()
    {
        $places = $this->queryPlacesMatchingCriteria();
        $places = $this->decoratePlacesWithDistance($places);
        $places = $this->assignScores($places);
        $places = $this->sortPlacesAccordingToCriteriaScores($places);

        $list = new RecommendationList();
        foreach($places as $place) {
            $list->add($place);
        }
        return $list;
    }

    /**
     * Given all criteria set on this Builder, query the Places API
     * and return a list of places.
     *
     * @return array Array of places.
     */
    private function queryPlacesMatchingCriteria()
    {
        // Since Places API doesn't keep atmosphere and cuisine data,
        // we'll try and use keywords to see if we can match it that way.
        $keyword = '';
        if($this->atmosphere !== null) {
            $keyword = $keyword . $this->atmosphere . ' ';
        }
        if($this->cuisine !== null) {
            $keyword = $keyword . $this->cuisine . ' ';
        }

        // Query the Places API
        $json = $this->placesHttp->nearby(
            $this->latitude,
            $this->longitude,
            trim($keyword) === '' ? null : $keyword,
            $this->isOpenNow,
            'prominence',
            'restaurant'
        );


        $places = $json->results;

        return $places;
    }


    /**
     * Calculate distance in metres between user's current position
     * and the position of each place.
     *
     * @param $places An array of places retrieved from the Places API
     * @return array Array of places with added distance (metres)
     */
    private function decoratePlacesWithDistance($places)
    {
        foreach($places as $place)
        {
            if(isset($place->geometry)) {
                $lat = $place->geometry->location->lat;
                $lng = $place->geometry->location->lng;

                $placeCoord = new Coordinate($lat, $lng);
                $myCoord = new Coordinate((float)$this->latitude, (float)$this->longitude);

                // Distance in meters
                $place->distance_from_me = $placeCoord->getDistance($myCoord, new Vincenty());
            }
        }
        return $places;
    }


    /**
     * Sort an array of places by score, such that the highest scoring place
     * is placed first in the array.
     *
     * @param array $places An array of places
     * @return array A sorted array of places
     */
    private function sortPlacesAccordingToCriteriaScores($places)
    {
        usort($places, ['OutToLunch\Classes\Recommendations\RecommendationBuilder', 'scoreSort']);

        return $places;
    }

    /**
     * Sort function comparing the score attribute on two objects.
     *
     * @param $a
     * @param $b
     * @return int
     */
    public static function scoreSort($a, $b)
    {
        if($a->score < $b->score) {
            return 1;
        }
        elseif($a->score > $b->score) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * Get the place that is furthest away from the user.
     *
     * @param array $places An array of places
     * @return object The place with the furthest distance in meters
     */
    private function getFurthestPlace($places)
    {
        if(count($places) < 1) {
            return null;
        }

        $furthestPlace = $places[0];
        foreach($places as $place) {
            if($place->distance_from_me > $furthestPlace->distance_from_me) {
                $furthestPlace = $place;
            }
        }
        return $furthestPlace;
    }

    /**
     * Gets an associative array of normalized (0-1) scores
     * for feedback given by past users. Given this current user inputs,
     * what is the likelihood, based on past user's inputs, that this place will be chosen?
     *
     * @param $places
     * @return An associative array of place_id => score
     */
    private function predictionScoresForPlaces($places)
    {
        $placeIds = [];
        foreach($places as $place) {
            $placeIds[] = $place->place_id;
        }

        $recommendations = Recommendation::whereIn('places_id', $placeIds)
            ->where('was_good_recommendation', 1)
            ->where('city', $this->city)
            ->where('distance_weight', $this->distance)
            ->where('quality_weight', $this->quality)
            ->where('price_weight', $this->price);

        if($this->atmosphere !== null && $this->atmosphere !== '') {
            $recommendations = $recommendations->where('atmosphere', $this->atmosphere);
        }
        if($this->cuisine !== null && $this->cuisine !== '') {
            $recommendations = $recommendations->where('cuisine', $this->cuisine);
        }

        $recommendations = $recommendations->get();

        $scores = [];
        // Set up the count for each recurrence
        foreach($recommendations as $recommendation) {
            if(!isset($scores[$recommendation->places_id])) {
                $scores[$recommendation->places_id] = 0;
            }

            $scores[$recommendation->places_id] = $scores[$recommendation->places_id] + 1;
        }

        // Divide each count by total recommendations to get normalized probability
        foreach($scores as $key => $val) {
            $scores[$key] = $scores[$key] / $recommendations->count();
        }

        return $scores;
    }

    /**
     * Assign scores to places based on the criteria supplied by the user.
     *
     * @param array $places An array of places
     * @return array An array of places with their scores attached
     */
    private function assignScores($places)
    {

        /*
         * Each category is given a score. This score is between 0-1 and is then
         * multiplied by a normalized weight for that category based on the user input.
         */
        $normalizedDistance = $this->distance;
        $normalizedQuality = $this->quality;
        $normalizedPrice = $this->price;
        $normalizedPredictions = $this->predictionScoresForPlaces($places);

        $nonNullCategories = 0;
        if($normalizedDistance !== null) {
            $nonNullCategories++;
        }
        if($normalizedQuality !== null) {
            $nonNullCategories++;
        }
        if($normalizedPrice !== null) {
            $nonNullCategories++;
        }

        if($normalizedDistance !== null) {
            $normalizedDistance = $normalizedDistance / $nonNullCategories;
        }
        if($normalizedQuality !== null) {
            $normalizedQuality = $normalizedQuality / $nonNullCategories;
        }
        if($normalizedPrice !== null) {
            $normalizedPrice = $normalizedPrice / $nonNullCategories;
        }

        foreach($places as $place)
        {
            $score = 0;
            if($normalizedDistance !== null) {
                $score += $this->calculateDistanceScore($place->distance_from_me, $normalizedDistance);
            }

            if($normalizedQuality !== null && isset($place->rating)) {
                $score += $this->calculateQualityScore($place->rating, $normalizedQuality);
            }

            if($normalizedPrice !== null) {
                $price = isset($place->price_level) ? $place->price_level : 1; // We will default to low price so as not to hurt rating
                $score += $this->calculatePriceScore($price, $normalizedPrice);
            }

            if(isset($normalizedPredictions[$place->place_id])) {
                $score += $normalizedPredictions[$place->place_id];
            }

            $place->score = $score;
        }

        return $places;
    }

    /**
     * Get normalized (0-1) score for the distance of the restaurant
     *
     * @param $distance Distance in meters from user
     * @param $weight The weight of distance compared to other factors
     * @return mixed
     */
    private function calculateDistanceScore($distance, $weight) {
        $x = $distance / ($this->isWalking ? self::MAX_WALKING_DISTANCE : self::MAX_DRIVING_DISTANCE);
        $score = max(
            0,
            -pow($x,2) + 1
        );
        $score *= $weight;
        return $score;
    }

    /**
     * Get normalized (0-1) score for the rating of the restaurant
     *
     * @param $rating Rating of the restaurant
     * @param $weight The weight of quality compared to other factors
     * @return mixed
     */
    private function calculateQualityScore($rating, $weight) {
        $score = $rating / self::MAX_RATING;
        $score *= $weight;

        return $score;
    }

    /**
     * Get normalized (0-1) score for the price level of the restaurant
     *
     * @param $price Price level of the restaurant
     * @param $weight The weight of price compared to other factors
     * @return mixed
     */
    private function calculatePriceScore($price, $weight) {
        $score = 1 - ($price / self::MAX_PRICE_LEVEL);
        $score *= $weight;

        return $score;
    }
}