<?php
/**
 * Created by PhpStorm.
 * User: justin
 * Date: 23/01/17
 * Time: 11:02 PM
 */

namespace OutToLunch\Classes\Recommendations;


class RecommendationList
{
    private $list = [];

    /**
     * RecommendationList constructor.
     * @param array $places
     */
    public function __construct($places = [])
    {
        $this->list = $places;
    }

    /**
     * Get the top recommendation, or null if there are no recommendations
     *
     * @return mixed
     */
    public function topRecommendation()
    {
        if(isset($this->list[0])) {
            return $this->list[0];
        } else {
            return null;
        }
    }

    /**
     * Add a new place to the list
     *
     * @param $place
     */
    public function add($place)
    {
        $this->list[] = $place;
    }

    /**
     * Get the list of places
     *
     * @return array
     */
    public function getList()
    {
        return $this->list;
    }
}