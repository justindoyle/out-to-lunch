<?php
namespace OutToLunch\Classes;

/**
 * Class GooglePlacesHttp
 *
 * Provides simple API calls to Google's Places API
 * using GuzzleHttp
 *
 * @package OutToLunch\Classes
 */
class GooglePlacesHttp
{
    private $client;
    private $key;

    public function __construct($key)
    {
        $this->client = new \GuzzleHttp\Client([
            'base_uri' => 'https://maps.googleapis.com/maps/api/place/'
        ]);
        $this->key = $key;
    }

    /**
     * Fetch a list of places (max 60) from the Places API using the Nearby search.
     * https://developers.google.com/places/web-service/search
     *
     *
     * @param $latitude
     * @param $longitude
     * @param null $keyword
     * @param bool $openNow
     * @param string $rankBy
     * @param string $type
     * @param null $pageToken
     * @param null $resultsToMergeWith
     * @return mixed
     */
    public function nearby(
        $latitude,
        $longitude,
        $keyword = null,
        $openNow = false,
        $rankBy = 'prominence',
        $type = 'restaurant',
        $pageToken = null,
        $resultsToMergeWith = null)
    {
        // Build out the query string
        $query = [
            'key' => $this->key,
            'location' => $latitude . ',' . $longitude,
            'rankby' => $rankBy,
            'type' => $type
        ];

        if($keyword !== null) {
            $query['keyword'] = $keyword;
        }
        if($pageToken !== null) {
            $query['pagetoken'] = $pageToken;
        }
        if($openNow === true) {
            $query['opennow'] = 1;
        }
        if($query['rankby'] !== 'distance') {
            $query['radius'] = env('GOOGLE_PLACES_SEARCH_RADIUS', 10000);
        }

        // Make the request
        $response = $this->client->request('GET', 'nearbysearch/json', [
            'query' => $query
        ]);


        $json = json_decode($response->getBody()->getContents());

        // If this merge is the result of fetching the next page in the results,
        // then we must merge the old results.
        if($resultsToMergeWith !== null && isset($json->results)) {
            $results = $json->results;
            $results = array_merge($resultsToMergeWith, $results);

            $json->results = $results;
        }

        // TODO Uncomment this if you want to gather as many results as possible and don't care about slow response times
//        // If there is another page of results, let's request it and merge it with what we already have
//        if(isset($json->next_page_token) && isset($json->results)) {
//            sleep(2);
//            // Sucks, but Google's makes you wait a few seconds before making the next_page_token valid
//            // If you don't wait, you will end up getting an INVALID_REQUEST returned. 2 seconds seems to be
//            // the smallest time that consistently returns a valid result.
//            //
//            // Places API doesn't allow you to get any more than 60 results per request batch anyways, so
//            // longest you would need to wait is 6 seconds. Not ideal, no good way around this right now.
//            $req = $this->nearby($latitude,
//                $longitude,
//                $keyword,
//                $openNow,
//                $rankBy,
//                $type,
//                $json->next_page_token,
//                $json->results);
//            return $req;
//        } else {
            return $json;
//        }
    }

    public function details($placeId)
    {
        // Build out the query string
        $query = [
            'key' => $this->key,
            'placeid' => $placeId
        ];

        $response = $this->client->request('GET', 'details/json', [
            'query' => $query
        ]);

        $json = json_decode($response->getBody()->getContents());

        return $json;
    }
}