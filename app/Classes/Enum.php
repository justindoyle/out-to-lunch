<?php
namespace OutToLunch\Classes;

abstract class Enum
{
    public static function isValidValue($value)
    {
        $reflection = new \ReflectionClass(get_called_class());
        $consts = $reflection->getConstants();

        return in_array($value, $consts);
    }
}