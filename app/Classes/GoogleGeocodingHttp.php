<?php
namespace OutToLunch\Classes;

/**
 * Class GoogleGeocodingHttp
 *
 * Provides simple API calls to Google's Geocoding API
 * using GuzzleHttp
 *
 * @package OutToLunch\Classes
 */
class GoogleGeocodingHttp
{
    private $client;
    private $key;

    public function __construct($key)
    {
        $this->client = new \GuzzleHttp\Client([
            'base_uri' => 'https://maps.googleapis.com/maps/api/geocode/json'
        ]);
        $this->key = $key;
    }


    public function getAddressForLocation($lat, $lng)
    {
        // Build out the query string
        $query = [
            'key' => $this->key,
            'latlng' => $lat . ',' . $lng
        ];

        $response = $this->client->request('GET', '', [
            'query' => $query
        ]);

        $json = json_decode($response->getBody()->getContents());

        return $json;
    }
}