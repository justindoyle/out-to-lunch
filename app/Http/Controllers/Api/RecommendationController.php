<?php

namespace OutToLunch\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use OutToLunch\CachedPlace;
use OutToLunch\Classes\GoogleGeocodingHttp;
use OutToLunch\Classes\GooglePlacesHttp;
use OutToLunch\Classes\Recommendations\RecommendationBuilder;
use OutToLunch\Http\Controllers\Controller;
use OutToLunch\Http\Requests\RecommendationFeedbackRequest;
use OutToLunch\Http\Requests\RecommendationGetRequest;
use OutToLunch\Recommendation;

class RecommendationController extends Controller
{
    /**
     * Get a list of recommendations based on user input
     *
     * @param RecommendationGetRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRecommendations(RecommendationGetRequest $request)
    {
        $lat = $request->input('lat');
        $lng = $request->input('lng');
        $urgency = $request->input('urgency');
        $budget = $request->input('budget');
        $transportationMethod = $request->input('transportation-method', 'walking');
        $cuisine = $request->input('cuisine', null);
        if($cuisine === 'all') {
            $cuisine = null;
        }
        $atmosphere = $request->input('atmosphere', null);
        if($atmosphere === 'all') {
            $atmosphere = null;
        }
        $city = $this->findCityForLocation($lat, $lng);

        $distanceWeight = $this->getDistanceWeight($urgency, $budget);
        $qualityWeight = $this->getQualityWeight($urgency, $budget);
        $priceWeight = $this->getPriceWeight($urgency, $budget);

        // Look for open places only when urgency is high
        $isOpen = strtolower($urgency) === 'high';

        $builder = new RecommendationBuilder(
            new GooglePlacesHttp(env('GOOGLE_API_KEY')),
            $lat,
            $lng,
            $city
        );

        $builder = $builder
            ->distance($distanceWeight)
            ->quality($qualityWeight)
            ->price($priceWeight)
            ->cuisine($cuisine)
            ->atmosphere($atmosphere)
            ->isOpenNow($isOpen);

        if($transportationMethod === 'walking') {
            $builder = $builder->amWalking();
        } else {
            $builder = $builder->amDriving();
        }

        $places = $builder
            ->get()
            ->getList();



        $places = Recommendation::saveResultsFromRecommendationsInput(
            $places,
            $city,
            $lat,
            $lng,
            $distanceWeight,
            $qualityWeight,
            $priceWeight,
            $cuisine,
            $atmosphere
        );

        return response()->json($places);
    }

    private function findCityForLocation($lat, $lng) {
        $googleGeocodeHttp = new GoogleGeocodingHttp(env('GOOGLE_API_KEY'));
        $address = $googleGeocodeHttp->getAddressForLocation($lat, $lng);
        $city = '';

        if($address->status === "OK") {
            $addressComponents = $address->results[0]->address_components;
            $relevantComponents = [
                'sublocality' => '',
                'locality' => '',
                'administrative_area_level_1' => '',
                'country' => ''
            ];

            // Match and fill any types found in the returned address data
            foreach($addressComponents as $addressComponent) {
                foreach($addressComponent->types as $type) {
                    if(array_has($relevantComponents, $type)) {
                        $relevantComponents[$type] = $addressComponent->short_name;
                    }
                }
            }


            // Clear any that weren't found
            foreach($relevantComponents as $key => $component) {
                if($component === '') {
                    unset($relevantComponents[$key]);
                }
            }

            $city = implode(', ', $relevantComponents);
        }

        return $city;
    }

    /**
     * Save whether or not the user felt a specific recommendation was helpful
     *
     * @param RecommendationFeedbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveRecommendationFeedback(RecommendationFeedbackRequest $request)
    {
        $recommendation = Recommendation::find($request->input('id'));

        if($recommendation->was_good_recommendation !== null) {
            return response()->json([
                'message' => 'Feedback was already given for this recommendation'
            ], 403);
        }

        $cachedPlace = $this->cachePlaceDetails($recommendation->places_id);
        $recommendation->cachedPlace()->associate($cachedPlace);
        $recommendation->was_good_recommendation = $request->input('was-good-recommendation');
        $recommendation->save();

        return response()->json([]);
    }

    private function cachePlaceDetails($placeId)
    {
        $cachedPlace = CachedPlace::where('place_id', $placeId)->first();

        if($cachedPlace === null) {
            $cachedPlace = new CachedPlace();
            $cachedPlace->place_id = $placeId;
        }

        if($cachedPlace->last_cached_at === null || $cachedPlace->cache_age > CachedPlace::MAX_CACHE_AGE) {
            $googlePlacesHttp = new GooglePlacesHttp(env('GOOGLE_API_KEY'));
            $details = $googlePlacesHttp->details($placeId);

            if($details->status === "OK") {
                $cachedPlace->last_cached_at = Carbon::now();
                $cachedPlace->cached_place = $details->result;
            }
        }

        $cachedPlace->save();

        return $cachedPlace;
    }



    private function getDistanceWeight($urgency, $budget)
    {
        $weight = 0;

        switch(strtolower($urgency))
        {
            case 'high':
                $weight += 3;
                break;
            case 'medium':
                $weight += 2;
                break;
            case 'low':
                $weight += 1;

        }

        return $weight;
    }

    private function getQualityWeight($urgency, $budget)
    {
        $weight = 0;

        switch(strtolower($budget))
        {
            case 'high':
                $weight += 3;
                break;
            case 'medium':
                $weight += 2;
                break;
            case 'low':
                $weight += 1;

        }

        return $weight;
    }

    private function getPriceWeight($urgency, $budget)
    {
        $weight = 0;

        switch(strtolower($budget))
        {
            case 'high':
                $weight += 1;
                break;
            case 'medium':
                $weight += 2;
                break;
            case 'low':
                $weight += 3;

        }

        return $weight;
    }
}
