<?php

namespace OutToLunch\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecommendationGetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'urgency' => 'required|string|in:low,medium,high',
            'budget' => 'required|string|in:low,medium,high',
            'transportation-method' => 'required|string|in:walking,driving',
            'cuisine' => 'string',
            'atmosphere' => 'string'
        ];
    }
}
