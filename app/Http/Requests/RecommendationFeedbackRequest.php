<?php

namespace OutToLunch\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use OutToLunch\Recommendation;

class RecommendationFeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(!$this->has('id') || !$this->has('response-token')) {
            return false;
        }

        $recommendation = Recommendation::where('id', $this->input('id'))
            ->where('response_token', $this->input('response-token'))
            ->first();


        return $recommendation === null ? false : true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'id' => 'required|exists:recommendations',
            'response-token' => 'required',
            'was-good-recommendation' => 'required|boolean'
        ];
    }
}
