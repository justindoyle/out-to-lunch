<?php

namespace OutToLunch;

use Illuminate\Database\Eloquent\Model;

class Recommendation extends Model
{


    public static function saveResultsFromRecommendationsInput($places, $city, $lat, $lng, $distanceWeight, $qualityWeight, $priceWeight, $cuisine, $atmosphere)
    {
        foreach($places as $place)
        {
            $recommendation = new Recommendation();
            $recommendation->places_id = $place->place_id;
            $recommendation->places_name = $place->name;
            $recommendation->lat = $lat;
            $recommendation->lng = $lng;
            $recommendation->city = $city;
            $recommendation->distance_weight = $distanceWeight;
            $recommendation->quality_weight = $qualityWeight;
            $recommendation->price_weight = $priceWeight;
            $recommendation->cuisine = $cuisine;
            $recommendation->atmosphere = $atmosphere;
            $recommendation->response_token = md5(uniqid(rand(), true)); // Not crypographically secure, but fine for out purposes

            $recommendation->save();

            // Enhance output with our new id, so the web user can use it to
            // give feedback on the recommendation
            $place->out_to_lunch_id = $recommendation->id;
            $place->out_to_lunch_response_token = $recommendation->response_token;
        }

        return $places;
    }

    public function cachedPlace() {
        return $this->belongsTo(CachedPlace::class, 'cached_place_id', 'id');
    }
}
