#!/bin/bash

# We need to install dependencies for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe


apt-get update -yqq
apt-get install git -yqq
apt-get install zlib1g-dev -yqq

# Install phpunit
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit

# Install php extensions
docker-php-ext-install zip
docker-php-ext-install mbstring
docker-php-ext-install pdo_mysql
