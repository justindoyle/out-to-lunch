<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use OutToLunch\Classes\GooglePlacesHttp;

use OutToLunch\Classes\Recommendations\RecommendationBuilder;
use OutToLunch\Classes\Recommendations\RecommendationList;

class RecommendationBuilderTest extends TestCase
{
    private $testPlaces;
    private $placesHttpStub;

    public function setUp()
    {
        $this->testPlaces = json_decode(file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . "test_places.json"));
        $this->placesHttpStub = $this->createMock(GooglePlacesHttp::class);
        $this->placesHttpStub->method('nearby')
            ->willReturn(
                (object)[
                    "results" => $this->testPlaces
            ]);
    }

    // TODO Commenting this out for now to keep from hitting daily API limit
//    public function testGetRecommendations()
//    {
//        $builder = new RecommendationBuilder(new GooglePlacesHttp(env('GOOGLE_API_KEY')), "42.9745301", "-81.2615451");
//        $list = $builder
//            ->get();
//
//        $this->assertInstanceOf(RecommendationList::class, $list);
//        $this->assertInternalType('array', $list->getList());
//    }

    public function testClosestRestaurant()
    {
        $builder = new RecommendationBuilder($this->placesHttpStub, "10", "10");
        $list = $builder
            ->distance(3)
            ->quality(0)
            ->price(0)
            ->get();

        $this->assertInstanceOf(RecommendationList::class, $list);
        $this->assertInternalType('array', $list->getList());

        $top = $list->topRecommendation();

        $this->assertInternalType('object', $top);
        $this->assertAttributeEquals(17, 'place_id', $top);
        $this->assertAttributeEquals(361, 'distance_from_me', $top);
    }

    public function testHighRatedRestaurant()
    {
        $builder = new RecommendationBuilder($this->placesHttpStub, "10", "10");
        $list = $builder
            ->distance(0)
            ->quality(3)
            ->price(0)
            ->get();

        $this->assertInstanceOf(RecommendationList::class, $list);
        $this->assertInternalType('array', $list->getList());

        $top = $list->topRecommendation();

        $this->assertInternalType('object', $top);
        $this->assertAttributeEquals(20, 'place_id', $top);
        $this->assertAttributeEquals(5.0, 'rating', $top);
    }

    public function testFastWithLowCostWhileDriving()
    {
        $builder = new RecommendationBuilder($this->placesHttpStub, "10", "10");
        $list = $builder
            ->distance(3)
            ->quality(1)
            ->price(3)
            ->amDriving()
            ->get();

        $this->assertInstanceOf(RecommendationList::class, $list);
        $this->assertInternalType('array', $list->getList());

        $top = $list->topRecommendation();

        $this->assertInternalType('object', $top);
        $this->assertAttributeEquals(13, 'place_id', $top);
        $this->assertAttributeEquals(608, 'distance_from_me', $top);
        $this->assertAttributeEquals(3.1, 'rating', $top);
    }

    public function testFastWithLowCostWhileWalking()
    {
        $builder = new RecommendationBuilder($this->placesHttpStub, "10", "10");
        $list = $builder
            ->distance(3)
            ->quality(1)
            ->price(3)
            ->amWalking()
            ->get();

        $this->assertInstanceOf(RecommendationList::class, $list);
        $this->assertInternalType('array', $list->getList());

        $top = $list->topRecommendation();

        $this->assertInternalType('object', $top);
        $this->assertAttributeEquals(17, 'place_id', $top);
        $this->assertAttributeEquals(361, 'distance_from_me', $top);
        $this->assertAttributeEquals(2.5, 'rating', $top);
    }

}
