<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use OutToLunch\Classes\Recommendations\Enums\Atmosphere;

class EnumTest extends TestCase
{
   public function testEnumValidConst()
   {
       $this->assertTrue(Atmosphere::isValidValue(Atmosphere::Date));
   }

    public function testEnumInvalidConst()
    {
        $this->assertFalse(Atmosphere::isValidValue('invalid'));
    }
}
