# Out To Lunch
#### A review for BNW
##### https://www.outtolunch.club

### Recommendations Interface 
### `GET /api/recommendation`
 I decided user inputs to find a recommendation should be as simple as possible, and let the API try to make most of the decisions. I separated user input into two types: "filtering" and "weighed".

Filtering inputs will attempt to filter results from the Google Places API. These filtered inputs are `atmosphere` and `cuisine`. Since the Google Places API doesn't offer a way to filter by these values specifically, my approach was to concatenate the values passed in for `atmosphere` and `cuisine` and pass them as part of the `keywords` filter that the Google Places API offers.
 
 Weighed inputs are integer inputs that determine how much weight to put on a specific feature of a restaurant while attempting to make a prediction. The weighed inputs are `urgency` and `budget`, and each range from 1-3 in weight (low to high). A higher `urgency` will put more weight on finding a place that is nearby; the highest `urgency` will filter for only restaurants that are currently marked as `open_now = true`. A high `budget` will put more weight on restaurants with a lower `price_level`, and put less weight on restaurants with a high `rating`, whereas a low `budget` will put less emphasis on a low `price_level` and also favor restaurants with a higher `rating`. 
 
 Separate from these inputs is also the `transportation-method` input, which can be `walking` or `driving`. This essentially helps the algorithm decide what distance would be considered "nearby".
 
### Recommendation Algorithm
 The algorithm first reaches out to Google Places API to get as many nearby restaurants as possible. Unfortunately Google will only return 20 results at a time, with a max of 3 pages (60 results in total). The other issue with this is that there a time limit placed on accessing the next page of your results, meaning you need to sleep for 2 seconds between each call to get your next page of results, resulting in poor response times for the Out To Lunch API. For now I have disabled paging the results to keep response times low. Using the filtering 
 
 The algorithm then creates a score between 0-1 for each category in `distance`, `price`, and `quality`. This is done by calculating a weight for each of these categories based in the user input, and multiplying it against data retrieved for the restaurant. These calculations are done as follow:
 - `distance`: `MAX(0, -x^2 + 1) * distanceWeight` where `x = DISTANCE_TO_RESTAURANT / MAX_DISTANCE` where `MAX_DISTANCE` depends on if the user chose `driving` or `walking` for their `transportation-method` input.
 - `price`: `1 - (RESTAURANT_PRICE_LEVEL / MAX_PRICE_LEVEL) * priceWeight`
 - `quality`: `RESTAURANT_RATING / MAX_RATING`
 
 A fourth `prediction` score between 0-1 will also be added on if, given the same user inputs and city, this restaurant has been recommended before and received positive user feedback (more on that later). As an example, if the user's input had been low budget, high urgency, walking, with cuisine = fast-food, and in the past 2 people had accepted Subway as the recommendation, and 1 person has accepted McDonald's, then Subway would get an addition `0.66` added to its score, and McDonald's would get `0.33` added, bringing their ranking a bit higher.
 
### Recommendation Feedback 
### `POST /api/recommendation/feedback`
 When users are given recommendations, these recommendations are stored in the database along with a `response_token`. Users can then call the api, passing along the `id` for the recommendation and `response_token` to indicate that they accepted this particular recommendation. Both the recommendation and all of the user inputs that resulted in that recommendation will be stored, and are used later in the scoring process of future recommendations to give better recommendation predictions.
 
### Future Considerations
 The fact that Google puts a timed rate limit on paging results, and a hard limit of only 60 results gives us a lot less to work with then we would sometimes like. Next steps would be to add a caching strategy to the results returned from Google Places API to enhance results for future queries.
 
 Right now when calculating the `prediction` score, the algorithm uses the `city` as one of the user inputs, instead of the `lat/lng` pair. This is because it's incredibly unlikely that two queries would have the exact same GPS coordinates, and would never result in a successful retrieval of past recommendations. Next steps would be to enhance this method so that instead of using `city` we would define a radius around the user's GPS position, and include any past recommendations with GPS coordinates inside of that radius to be included for calculating the `prediction` score.
 
 Another problem with the Google Places API is that in order to get the opening hours of a business, you need to individually query each result returned by the `nearby` search for the details of that restaurant. Doing this every time would pretty soon set you against your rate limit, and could run up costs. It would be helpful to include a caching strategy, as discussed earlier, so that this information could be stored for later use. This way we could also include the time range that a user will be wanting to eat at, and can filter out any restaurants that wouldn't be open during that time frame.
 
### Code Files
 For the code review, the relevant files can be found here:
 - `app/Classes/*`. Any files found here
 - `app/Http/Controllers/Api/RecommendationController`
 - `resources/assets/js/index.jsx` This is the entrypoint to the React app for the frontend. Any further code can be found in `OutToLunch.api` and anything under `react/*`
 
 
### Database Structure

Tables:
- `recommendations`
 - `id`: integer unsigned unique primary auto increment
 - `places_id`: varchar(256) index - The `place_id` field supplied from Google Places API
 - `places_name`: varchar(256) default '' - The `name` field supplied from Google Places API
 - `lat`: decimal(11, 8) not null
 - `lng`: decimal(11, 8) not null
 - `city`: varchar(256) not null - Store the city/state/country that the user was in when they asked for a recommendation (for example Halifax, NS, CA)
 - `distance_weight`: int not null default 0 - The weight for distance used to get this recommendation
 - `quality_weight`: int not null default 0 - The weight for quality used to get this recommendation
 - `price_weight`: int not null default 0 - The weight for price used to get this recommendation
 - `cuisine`: varchar(256) - The cuisine keyword string used to get this recommendation
 - `atmosphere`: varchar(256) - The atmosphere keyword string used to get this recommendation
 - `response_token`: varchar(256) not null - The response token that the user can use to give feedback on this recommendation
 - `was_good_recommendation`: tinyint - A boolean value saying whether the feeback response was 0 (negative) or 1 (positive)
 - `updated_at` datetime not null
 - `created_at` datetime not null
 - `cached_place_id` int unsigned - A reference to the `id` field in the `cached_places` table
 - `recommendations_cached_place_id_index` foreign key on delete set null - Foreign key index for the `id` field in `cached_places` 

- `cached_places`
 - `id`: integer unsigned unique primary auto increment
 - `place_id`: varchar(256) index - The `place_id` field supplied from Google Places API
 - `cached_place`: json not null default null (json type null, not MySQL NULL) - A cached version of the json object returned by Google Places API
 - `last_cached_at`: datetime not null - The last time this place was updated from the Google Places API
 - `updated_at`: datetime not null
 - `created_at`: datetime not null

