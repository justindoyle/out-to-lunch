var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'public/js');
var APP_DIR = path.resolve(__dirname, 'resources/assets/js');

var config = {
    entry: [
        APP_DIR + '/index.jsx'
        ],
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?/,
                include: APP_DIR,
                loaders: ['babel-loader']
            }
        ]
    },

    plugins: [
        new webpack.ProvidePlugin({
            Promise: 'es6-promise'
        })
    ]
};

module.exports = config;



