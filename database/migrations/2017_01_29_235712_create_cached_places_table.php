<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCachedPlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cached_places', function (Blueprint $table) {
            $table->increments('id');

            $table->string('place_id')->index()->unique();
            $table->json('cached_place')->default(null);
            $table->timestamp('last_cached_at');

            $table->timestamps();
        });


        Schema::table('recommendations', function(Blueprint $table) {
            $table->integer('cached_place_id')->unsigned()->nullable();
            $table->index('cached_place_id')
                ->referenced('id')
                ->on('cached_places')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recommendations', function(Blueprint $table) {
            $table->dropIndex('recommendations_cached_place_id_index');
            $table->dropColumn('cached_place_id');
        });
        Schema::dropIfExists('cached_places');
    }
}
