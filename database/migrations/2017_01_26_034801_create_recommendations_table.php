<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecommendationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommendations', function (Blueprint $table) {
            $table->increments('id');

            $table->string('places_id')->index();
            $table->string('places_name')->default('');
            $table->double('lat');
            $table->double('lng');
            $table->string('city');
            $table->integer('distance_weight')->default(0);
            $table->integer('quality_weight')->default(0);
            $table->integer('price_weight')->default(0);
            $table->string('cuisine')->nullable();
            $table->string('atmosphere')->nullable();
            $table->string('response_token');
            $table->boolean('was_good_recommendation')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recommendations');
    }
}
