require('es6-promise').polyfill(); // Globally polyfill ES6 Promises

import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux'
import { Provider, connect } from 'react-redux'
import RecommendFormContainer from './react/containers/RecommendFormContainer'
import RecommendationContainer from './react/containers/RecommendationContainer'
import RecommendationAccepted from './react/components/RecommendationAccepted'
import LoadingIcon from './react/components/LoadingIcon'
import { Router, Route, Link, IndexRoute, browserHistory } from 'react-router'
import { syncHistoryWithStore, routerMiddleware, push } from 'react-router-redux'
import thunk from 'redux-thunk';
import reducers from './react/reducers'

const store = createStore(
    reducers,
    applyMiddleware(
        routerMiddleware(browserHistory),
        thunk
    )
);
const history = syncHistoryWithStore(browserHistory, store);

const OutToLunch = (props) => {
    const renderHeading = () => {
        if (props.app.headerText !== undefined && props.app.headerText !== ''
            && props.app.subheaderText !== undefined && props.app.subheaderText !== '') {
            return (
                <div className="panel-heading">
                    <h1>{props.app.headerText}</h1>
                    <h3 style={{paddingLeft: 10}}>{props.app.subheaderText}</h3>
                </div>
            )
        } else {
            return null;
        }
    }

    const renderLoadingIcon = (isLoading, loadingText) => {
        if(!isLoading) {
            return null;
        }

        return (
            <div className="loading-icon-wrapper">
                <div className="loading-icon">
                    <LoadingIcon />
                    <h1 style={{color: 'white'}}>{loadingText}</h1>
                </div>
            </div>
        )
    }

    return (
        <div style={{height: '100%', width: '100%'}}>
            <div className={"panel panel-primary " + props.app.panelClass}>
                {renderHeading()}
                <div className="panel-body" style={{position: 'relative'}}>
                    {renderLoadingIcon(props.app.isLoading, props.app.loadingText)}
                    {props.children}
                </div>
            </div>
        </div>
    )
}

const appMapStateToProps = (state, ownProps) => {

    return {
        app: state.app
    }
};

const appMapDispatchToProp = (dispatch, ownProps) => {
    return {}
}
const OutToLunchContainer = connect(
    appMapStateToProps,
    appMapDispatchToProp
)(OutToLunch);

ReactDOM.render((
        <Provider store={store}>
            <Router history={history}>
                <Route path="/" component={OutToLunchContainer}>
                    <IndexRoute component={RecommendFormContainer}/>
                    <Route path="recommendation" component={RecommendationContainer} />
                    <Route path="recommendation/accepted" component={RecommendationAccepted} />
                    <Route path="*" component={FourOhFour}/>
                </Route>
            </Router>
        </Provider>
    ),
    document.getElementById('react-root')
);


const FourOhFour = () => (
    <div>
        <h1>404: The page you were looking for doesn't exist!</h1>
    </div>
)
