import {
    APP_ACTIONS
} from './types'
import 'whatwg-fetch'


/**
 * Set the header in the app's panel
 * @param text
 * @returns {{type: string, text: *}}
 */
export const setHeaderText = (text) => {
    return {
        type: APP_ACTIONS.SET_HEADER_TEXT,
        text: text
    }
};


/**
 * Set the subheader in the app's panel
 * @param text
 * @returns {{type: string, text: *}}
 */
export const setSubheaderText = (text) => {
    return {
        type: APP_ACTIONS.SET_SUBHEADER_TEXT,
        text: text
    }
};

/**
 * Set the class for the app's panel
 * @param className
 * @returns {{type: string, className: *}}
 */
export const setPanelClass = (className) => {
    return {
        type: APP_ACTIONS.SET_PANEL_CLASS,
        className: className
    }
};

/**
 * Set whether or not the app is currently loading
 * @param isLoading
 * @param loadingText The text to display while loading
 * @returns {{type: string, isLoading: *, loadingText: *}}
 */
export const setLoadingStatus = (isLoading, loadingText) => {
    return {
        type: APP_ACTIONS.SET_LOADING_STATUS,
        isLoading: isLoading,
        loadingText: loadingText
    }
};

/**
 * Set the latitude and longitude for the user
 * @param lat
 * @param lng
 * @param address
 * @returns {{type: string, lat: *, lng: *, address: *}}
 */
export const setLocation = (lat, lng, address) => {
    return {
        type: APP_ACTIONS.SET_LOCATION,
        lat: lat,
        lng: lng,
        address: address
    }
};

/**
 * Set the lat/lng for the app based on the address supplied. Uses
 * Google's Geocoding API
 * @param address
 * @returns {Function}
 */
export const getLocationForAddress = (address) => {
    return (dispatch, getState) => {
        let p = new Promise((resolve, reject) => {
            fetch(buildAddressGeocodingUrl(address))
                .then(response => {
                    response.json()
                        .then(json => {
                            if (response.ok) {
                                resolve({response, json});
                            } else {
                                reject({response, json});
                            }
                        })
                        .catch(fail => {
                            console.log(response);
                            console.log(fail);
                            reject(fail);
                        })
                })
                .catch(fail => {
                    console.log("Request failed: " + encodedUri);
                    reject(fail);
                })
        })
        return p.then(success => {
            if (success.json.status !== "OK") {
                console.log("Failed getting geocoding");
                return Promise.reject(success.json);
            }

            let location = success.json.results[0].geometry.location;
            dispatch(setLocation(location.lat, location.lng, address));
            return Promise.resolve(success);
        })
    }
}

/**
 * Set the address for the app based on the lat/lng supplied. Uses
 * Google's Geocoding API
 * @param address
 * @returns {Function}
 */
export const getAddressForLocation = (lat, lng) => {
    return (dispatch, getState) => {
        let p = new Promise((resolve, reject) => {
            fetch(buildReverseGeocodingUrl(lat, lng))
                .then(response => {
                    response.json()
                        .then(json => {
                            if (response.ok) {
                                resolve({response, json});
                            } else {
                                reject({response, json});
                            }
                        })
                        .catch(fail => {
                            console.log(response);
                            console.log(fail);
                            reject(fail);
                        })
                })
                .catch(fail => {
                    console.log("Request failed: " + encodedUri);
                    reject(fail);
                })
        })
        return p.then(success => {
            if (success.json.status !== "OK") {
                console.log("Failed getting reverse geocoding");
                return Promise.reject(success.json);
            }

            let address = success.json.results[0].formatted_address;
            dispatch(setLocation(lat, lng, address));
            return Promise.resolve(success);
        })
    }
}



const googleApiKey = "AIzaSyBscAwZMDsYAermZsui28bGgr5LaBU2x_M";

const buildAddressGeocodingUrl = (address) => {
    return "https://maps.googleapis.com/maps/api/geocode/json?address=" + encodeURIComponent(address) + "&key=" + encodeURIComponent(googleApiKey);
}

const buildReverseGeocodingUrl = (lat, lng) => {
    return "https://maps.googleapis.com/maps/api/geocode/json?latlng="+encodeURIComponent(lat)+","+encodeURIComponent(lng)+"&key=" + encodeURIComponent(googleApiKey);
}