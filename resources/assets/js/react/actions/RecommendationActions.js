import {
    RECOMMENDATION_ACTIONS
} from './types'
import OutToLunchApi from '../../OutToLunchApi'

/**
 * Load the recommendations from the OutToLunch REST API
 * @param params
 * @returns {Function}
 */
export const load = (params) => {
    return (dispatch, getState) => {
        let api = new OutToLunchApi();

        return api.recommendations(params.lat,
            params.lng,
            params.urgency,
            params.budget,
            params.transportationMethod,
            params.cuisine,
            params.atmosphere)
            .then(success => {
                dispatch(setRecommendations(success.json));
                return Promise.resolve();
            });
    };
}

/**
 * Mark a recommendation as being accepted.
 *
 * @param recommendation
 * @returns {Function}
 */
export const acceptRecommendation = (recommendation) => {
    return (dispatch, getState) => {
        let api = new OutToLunchApi();

        return api.acceptRecommendation(recommendation.out_to_lunch_id, recommendation.out_to_lunch_response_token);
    }
}

export const setRecommendations = (recommendations) => {
    return {
        type: RECOMMENDATION_ACTIONS.SET_LIST,
        recommendations: recommendations
    }
};

export const setRecommendationCriteria = (criteria) => {
    return {
        type: RECOMMENDATION_ACTIONS.SET_CRITERIA,
        criteria: criteria
    }
}