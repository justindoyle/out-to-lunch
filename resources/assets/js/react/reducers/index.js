import { combineReducers } from 'redux'
import { routerReducer }  from 'react-router-redux'
import recommendations from './recommendations'
import app from './app'

const reducers = combineReducers({
    routing: routerReducer,
    recommendations: recommendations,
    app: app
});
export default reducers;