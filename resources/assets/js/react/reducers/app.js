import React from 'react'
import {
    APP_ACTIONS
} from '../actions/types'

const app = (state = defaultState, action) => {
    switch(action.type) {
        case APP_ACTIONS.SET_HEADER_TEXT: return setHeaderText(state, action);
        case APP_ACTIONS.SET_SUBHEADER_TEXT: return setSubheaderText(state, action);
        case APP_ACTIONS.SET_PANEL_CLASS: return setPanelClass(state, action);
        case APP_ACTIONS.SET_LOADING_STATUS: return setLoadingStatus(state, action);
        case APP_ACTIONS.SET_LOCATION: return setLocation(state, action);
        default: return state;
    }
};
export default app;

const defaultState = {
    isLoading: false,
    loadingText: 'Finding Food...',
    headerText: 'Out to Lunch',
    subheaderText: 'Find me a place!',
    panelClass: 'find-recommendation-panel',
    location: {
        lat: null,
        lng: null,
        address: ''
    }
}

const setHeaderText = (oldState, action) => {
    let state = Object.assign({}, oldState);

    if(typeof action.text === 'string') {
        state.headerText = action.text;
    }

    return state;
}

const setSubheaderText = (oldState, action) => {
    let state = Object.assign({}, oldState);

    if(typeof action.text === 'string') {
        state.subheaderText = action.text;
    }

    return state;
}

const setPanelClass = (oldState, action) => {
    let state = Object.assign({}, oldState);

    if(typeof action.className === 'string') {
        state.panelClass = action.className;
    }

    return state;
}

const setLoadingStatus = (oldState, action) => {
    let state = Object.assign({}, oldState);

    if(typeof action.isLoading === 'boolean') {
        state.isLoading = action.isLoading;
    }

    if(typeof action.loadingText === 'string') {
        state.loadingText = action.loadingText;
    }

    return state;
}

const setLocation = (oldState, action) => {
    let state = Object.assign({}, oldState);

    state.location.lat = action.lat;
    state.location.lng = action.lng;
    state.location.address = action.address;

    return state;
}