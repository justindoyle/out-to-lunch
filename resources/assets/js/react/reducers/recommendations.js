import {
    RECOMMENDATION_ACTIONS
} from '../actions/types'

const recommendations = (state = defaultState, action) => {
    switch(action.type) {
        case RECOMMENDATION_ACTIONS.SET_LIST: return setList(state, action);
        case RECOMMENDATION_ACTIONS.SET_CRITERIA: return setCriteria(state, action);
        default: return state;
    }
};
export default recommendations;

const defaultState = {
    list: [],
    criteria: {
        urgency: 'high',
        budget: 'medium',
        transportationMethod: 'walking',
        cuisine: 'any',
        atmosphere: 'any'
    }
}

const setList = (oldState, action) => {
    let state = Object.assign({}, oldState);

    if(Array.isArray(action.recommendations)) {
        state.list = action.recommendations;
    }

    return state;
}

const setCriteria = (oldState, action) => {
    let state = Object.assign({}, oldState);

    let criteria = Object.assign({}, defaultState.criteria);
    for(let c in criteria) {
        if(action.criteria[c] !== undefined) {
            criteria[c] = action.criteria[c];
        }
    }
    state.criteria = criteria;

    return state;
}