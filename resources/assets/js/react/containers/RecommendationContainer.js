import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import Recommendation from '../components/Recommendation'
import {
    setHeaderText,
    setSubheaderText,
    setPanelClass
} from '../actions/AppActions'
import {
    acceptRecommendation
} from '../actions/RecommendationActions'

const mapStateToProps = (state, ownProps) => {

    console.log(state);
    let recommendations = state.recommendations.list;

    return {
        recommendations: recommendations
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {

    return {
        setupAppPanel: (header, subheader, className) => {
            dispatch(setHeaderText(header));
            dispatch(setSubheaderText(subheader));
            dispatch(setPanelClass(className));
        },

        onAcceptRecommendation: (recommendation) => {
            dispatch(acceptRecommendation(recommendation));
            dispatch(push('/recommendation/accepted'));
        }
    }
}


const RecommendationContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Recommendation)

export default RecommendationContainer
