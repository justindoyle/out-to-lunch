import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import RecommendForm from '../components/RecommendForm'
import {
    load as loadRecommendations,
    setRecommendationCriteria
} from '../actions/RecommendationActions'
import {
    setHeaderText,
    setSubheaderText,
    setPanelClass,
    setLoadingStatus,
    setLocation,
    getLocationForAddress,
    getAddressForLocation
} from '../actions/AppActions'

const mapStateToProps = (state, ownProps) => {
    const urgencyList = {
        high: 'Food! Now!',
        medium: 'I could eat soon',
        low: 'Let\'s eat later'
    };
    const budgetList = {
        high: 'I\'m on a budget',
        medium: 'Value for money',
        low: 'Spare no expense!'
    };
    const cuisineList = {
        'any': 'Any',
        'american': 'American',
        'cajun': 'Cajun',
        'chinese': 'Chinese',
        'greek': 'Greek',
        'indian': 'Indian',
        'italian': 'Italian',
        'japanese': 'Japanese',
        'korean': 'Korean',
        'lebanese': 'Lebanese',
        'mexican': 'Mexican',
        'soul food': 'Soul Food',
        'thai': 'Thai'
    };
    const atmosphereList = {
        'any': 'Any',
        'casual': 'Casual',
        'romantic': 'Date Night',
        'family': 'Family Friendly'
    };

    return {
        location: state.app.location,
        urgencyList: urgencyList,
        budgetList: budgetList,
        cuisineList: cuisineList,
        atmosphereList: atmosphereList,
        criteria: state.recommendations.criteria,
        isLoading: state.app.isLoading
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {

    return {
        setupAppPanel: (header, subheader, className) => {
            dispatch(setHeaderText(header));
            dispatch(setSubheaderText(subheader));
            dispatch(setPanelClass(className));
        },

        setLocation: (lat, lng, address) => {
            dispatch(setLocation(lat, lng, address));
        },

        setAddressForLocation: (lat, lng) => {
            dispatch(setLoadingStatus(true, 'Looking up Address...'));

            return dispatch(getAddressForLocation(lat, lng))
                .then(success => {
                    dispatch(setLoadingStatus(false, ''));
                    return Promise.resolve(success);
                })
                .catch(fail => {
                    dispatch(setLoadingStatus(false, ''))
                    return Promise.reject(fail);
                });
        },

        onClickGetLocationForAddress: (address) => {
            dispatch(setLoadingStatus(true, 'Looking up Address...'));

            return dispatch(getLocationForAddress(address))
                .then(success => {
                    dispatch(setLoadingStatus(false, ''));
                    return Promise.resolve(success);
                })
                .catch(fail => {
                    dispatch(setLoadingStatus(false, ''))
                    return Promise.reject(fail);
                });
        },

        onClickRecommend: (params) => {
            dispatch(setLoadingStatus(true, 'Finding Food...'));
            dispatch(setRecommendationCriteria(params));

            return dispatch(loadRecommendations(params))
                .then(success => {
                    dispatch(push('/recommendation'));
                    dispatch(setLoadingStatus(false, ''));
                    return Promise.resolve();
                })
                .catch(fail => {
                    console.log('Failed loading recommendations');
                    console.log(fail);
                    dispatch(setLoadingStatus(false, ''));
                    return Promise.reject(fail);
                })
        },

        setCriteria: (criteria) => {
            dispatch(setRecommendationCriteria(criteria));
        },

        setLoadingStatus: (isLoading, loadingText) => {
            dispatch(setLoadingStatus(isLoading, loadingText));
        }
    }
}


const RecommendFormContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(RecommendForm)

export default RecommendFormContainer
