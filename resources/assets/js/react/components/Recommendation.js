import React from 'react'
import GoogleMap from 'google-map-react'
import MapMarker from './MapMarker'
import { Link } from 'react-router'

export default class Recommendation extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            recommendationIndex: 0
        }
    }

    componentDidMount() {
        this.setupAppPanel();
    }

    /**
     * Set the header, subheader, and class for the app panel
     */
    setupAppPanel() {
        this.props.setupAppPanel(
            this.props.recommendations[this.state.recommendationIndex].name,
            'Our best guess for you',
            'predicted-recommendation-panel'
        )
    }

    onClickGoodRecommendation() {
        this.props.onAcceptRecommendation(this.props.recommendations[this.state.recommendationIndex]);
    }

    onClickBadRecommendation() {
        if(this.state.recommendationIndex + 1 < this.props.recommendations.length - 1) {
            this.setState({
                recommendationIndex: this.state.recommendationIndex + 1
            }, () => {
                this.setupAppPanel();
            })
        }
    }

    renderPriceLevel(priceLevel) {
        if(priceLevel === undefined) {
            return <span className="text-muted">??</span>
        }

        let filledSigns = <span className="text-success" style={{fontSize: 18}}>{Array(priceLevel + 1).join('$')}</span>
        let emptySigns = <span style={{fontSize: 18, color: '#bbb'}}>{Array(4 - priceLevel + 1).join('$')}</span>

        return (
            <div>
                {emptySigns}{filledSigns}
            </div>
        )
    }

    renderMap(recommendation) {
        if(recommendation.geometry === undefined || recommendation.geometry.location === undefined) {
            return <p className="text-muted">No map data available</p>
        }

        let lat = recommendation.geometry.location.lat;
        let lng = recommendation.geometry.location.lng;
        let zoom = 15;

        return (
            <div style={{width: '100%', height: '100%', minHeight: 400, margin: 0, padding: 0}}>
                <GoogleMap
                    apiKey={'AIzaSyBscAwZMDsYAermZsui28bGgr5LaBU2x_M'}
                    style={{width: '100%', height: '100%', minHeight: 400, margin: 0, padding: 0}}
                    center={{lat: lat, lng: lng}}
                    zoom={zoom}>
                    <MapMarker lat={lat} lng={lng} imgSrc={recommendation.icon} />
                </GoogleMap>
            </div>
        )

    }

    renderInfoTable(recommendation) {
        const humanDistance = (distance) => {
            if (distance < 1000) {
                return distance.toFixed(0) + 'm';
            } else {
                return (distance / 1000).toFixed(1) + 'km';
            }
        }

        const isOpenNow = (recommendation) => {
            if(recommendation.opening_hours === undefined || recommendation.opening_hours.open_now === undefined) {
                return <span>??</span>
            }
            return (recommendation.opening_hours.open_now ?
                <span className="text-success">Yes</span>
                : <span className="text-danger">No</span>)
        }

        return (
            <table className="table table-striped table-hover">
                <tbody>
                <tr>
                    <td>Distance</td>
                    <td>{humanDistance(recommendation.distance_from_me)}</td>
                </tr>
                <tr>
                    <td>Rating</td>
                    <td>{recommendation.rating + ' / 5'}</td>
                </tr>
                <tr>
                    <td>Price Level</td>
                    <td>{this.renderPriceLevel(recommendation.price_level)}</td>
                </tr>
                <tr>
                    <td>Open Now?</td>
                    <td>{isOpenNow(recommendation)}</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>{recommendation.vicinity}</td>
                </tr>
                </tbody>
            </table>
        )
    }

    render() {

        let r = this.props.recommendations[this.state.recommendationIndex];

        if(r === undefined) {
            return (
                <div>
                    <p className="text-muted">
                        Looks like we couldn't find anything! Pick other options and try again.
                    </p>
                    <Link className="btn btn-primary" to="/">Back</Link>
                </div>
            )
        }

        return (
            <div>
                <div className="col-md-6">
                    {this.renderInfoTable(r)}
                </div>
                <div className="col-md-6">
                    {this.renderMap(r)}
                </div>
                <br/>
                Was this a good recommendation?
                <br/>
                <button className="btn btn-success" style={{marginRight: 10}} type="button" onClick={this.onClickGoodRecommendation.bind(this)}>Yes</button>
                <button className="btn btn-default" type="button" onClick={this.onClickBadRecommendation.bind(this)}>No</button>
            </div>
        )
    }
}
