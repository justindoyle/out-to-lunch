import React from 'react'

const FormGroup = (props) => {

    let groupClass = ['form-group'];
    if(props.hasError) {
        groupClass.push('has-error');
    }
    if(props.hasWarning) {
        groupClass.push('has-warning');
    }

    return (
        <div className={groupClass.join(' ')}>
            <label type="text" className="col-lg-2 control-label">
                {props.label}
            </label>
            <div className="col-lg-10">
                {props.children}
            </div>
        </div>
    )
};
export default FormGroup;