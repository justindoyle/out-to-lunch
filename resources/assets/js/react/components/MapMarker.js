import React from 'react'


const MapMarker = (props) => {
    return (
        <div className="recommendation-map-marker">
            <img src={props.imgSrc} alt=""/>
        </div>
    )
}
export default MapMarker;