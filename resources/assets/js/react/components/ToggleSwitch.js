import React, { Component } from 'react'

const ToggleSwitch = (props) => {


    return (
        <div className="switch-toggle well">

            <input
                id={props.left.name}
                value={props.left.value}
                name={props.name}
                checked={props.selected === props.left.value}
                type="radio"
                disabled={props.disabled}
            />
            <label className={props.selected === props.left.value ? 'selected': ''} htmlFor={props.left.name}
                   onClick={props.left.onClick}>{props.left.label}</label>

            <input
                id={props.right.name}
                value={props.right.value}
                name={props.name}
                checked={props.selected === props.right.value}
                type="radio"
                disabled={props.disabled}
            />
            <label className={props.selected === props.right.value ? 'selected': ''} htmlFor={props.right.name}
                   onClick={props.right.onClick}>{props.right.label}</label>

            <a className="btn btn-primary"></a>
        </div>
    )
};
export default ToggleSwitch;

ToggleSwitch.propTypes = {
    left: React.PropTypes.shape({
        label: React.PropTypes.any.isRequired,
        value: React.PropTypes.any.isRequired,
        onClick: React.PropTypes.func.isRequired
    }).isRequired,
    right: React.PropTypes.shape({
        label: React.PropTypes.any.isRequired,
        value: React.PropTypes.any.isRequired,
        onClick: React.PropTypes.func.isRequired
    }).isRequired,
    selected: React.PropTypes.any,
    name: React.PropTypes.string.isRequired
}