import React from 'react'
import FormGroup from './FormGroup'
import ToggleSwitch from './ToggleSwitch'
import LoadingIcon from './LoadingIcon'

export default class RecommendForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            location: null,
            transportationMethod: props.criteria.transportationMethod,
            errorMessage: ''
        }
    }

    componentDidMount() {
        this.props.setupAppPanel('Out To Lunch', 'Find me a place!', 'find-recommendation-panel');
        this.refs.address.value = this.props.location.address;
    }

    getLocation() {
        // Use the browser's geolocation api (user must allow, so entering address is allowed as backup)
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                this.onLocationLoaded.bind(this),
                this.showError.bind(this),
                {
                    enableHighAccuracy: false,
                    timeout: 5000,
                    maximumAge: 0
                }
            );
        } else {
            console.log("Geolocation is not supported by this browser.");
        }
    }

    /**
     * Callback for when navigator.geolocation.getCurrentPosition is successful
     * @param location
     */
    onLocationLoaded(location) {
        this.setState({
            errorMessage: ''
        });

        this.props.setAddressForLocation(
            location.coords.latitude,
            location.coords.longitude
            )
            .then(success => {
                this.refs.address.value = success.json.results[0].formatted_address
            })
    }

    /**
     * Callback for when navigator.geolocation.getCurrentPosition fails
     * @param error
     */
    showError(error) {
        let errorMessage = '';
        switch (error.code) {
            case error.PERMISSION_DENIED:
                errorMessage = "User denied the request for Geolocation.";
                break;
            case error.POSITION_UNAVAILABLE:
                errorMessage = "Location information is unavailable.";
                break;
            case error.TIMEOUT:
                errorMessage = "The request to get user location timed out.";
                break;
            default:
                errorMessage = "An unknown error occurred.";
                break;
        }
        this.setState({
            errorMessage: errorMessage
        })
    }

    /**
     * Load the recommendations based on the user's input
     * @param e
     */
    onClickRecommend(e) {
        e.preventDefault();
        let location = this.props.location;
        if(typeof location.lat !== 'number' || typeof location.lng !== 'number') {
            this.setState({
                errorMessage: 'No address has been chosen!'
            });
            return;
        }
        this.setState({
            errorMessage: ''
        });

        this.props.onClickRecommend({
            lat: this.props.location.lat,
            lng: this.props.location.lng,
            urgency: this.props.criteria.urgency,
            budget: this.props.criteria.budget,
            transportationMethod: this.props.criteria.transportationMethod,
            cuisine: this.props.criteria.cuisine,
            atmosphere: this.props.criteria.atmosphere
        })
    }

    /**
     * Set the app state's user input criteria given
     * the values of the inputs in this form.
     * @param e
     */
    updateCriteria(e) {

        this.props.setCriteria({
            lat: this.props.location.lat,
            lng: this.props.location.lng,
            urgency: this.refs.urgency.value,
            budget: this.refs.budget.value,
            transportationMethod: this.state.transportationMethod,
            cuisine: this.refs.cuisine.value,
            atmosphere: this.refs.atmosphere.value
        });
    }

    /**
     * Update the transportation method when the Transportation Method switch is toggled
     * @param method
     */
    updateTransportationMethod(method) {
        this.setState({
            transportationMethod: method
        }, () => {
            this.updateCriteria()
        })
    }

    /**
     * Load GPS lat and lng based on the address types into the address field
     */
    onClickGetLocationForAddress() {
        this.props.onClickGetLocationForAddress(this.refs.address.value)
            .then(success => {
                this.setState({
                    errorMessage: ''
                })
            })
            .catch(fail => {
                this.setState({
                    errorMessage: 'Could not retrieve GPS location for address. Try again, or try another address'
                });
            })
    }

    onBlurAddressField() {
        let address = this.refs.address.value;

        if(address !== undefined && address !== null && address !== '') {
            this.onClickGetLocationForAddress();
        }
    }

    renderSelectOptions(options) {
        let r = [];
        for (let o in options) {
            r.push(<option key={o} value={o}>{options[o]}</option>);
        }
        return r;
    }


    render() {
        let location = this.props.location;
        let addressValue = '';

        if (location !== null && typeof location.lat === 'number' && typeof location.lat === 'number') {
            addressValue = addressValue
                + location.lat
                + ','
                + location.lat;
        }

        return (
            <div style={{position: 'relative'}}>
                <form className="form-horizontal">
                    <fieldset>
                        <p className="text-danger">
                            {this.state.errorMessage}
                        </p>
                        <FormGroup label={'Address'}>
                            <input ref={'address'} type="text" className="form-control"
                                   onBlur={this.onBlurAddressField.bind(this)}
                                   disabled={this.props.isLoading}/>
                        </FormGroup>
                        <div className="col-lg-10 col-lg-offset-2">
                            <button type="button" className={'btn btn-default'} style={{marginBottom:10}}
                                    onClick={e => this.getLocation()} disabled={this.props.isLoading}>Use current
                                position
                            </button>
                            <p className="text-muted small">GPS Coordinates: {addressValue}</p>
                        </div>
                        <FormGroup label={'When?'}>
                            <select ref={'urgency'} name="urgency" id="urgency-select" className="form-control"
                                    value={this.props.criteria.urgency} onChange={this.updateCriteria.bind(this)}
                                    disabled={this.props.isLoading}>
                                {this.renderSelectOptions(this.props.urgencyList)}
                            </select>
                        </FormGroup>
                        <FormGroup label={'Budget?'}>
                            <select ref={'budget'} name="budget" id="budget-select" className="form-control"
                                    value={this.props.criteria.budget} onChange={this.updateCriteria.bind(this)}
                                    disabled={this.props.isLoading}>
                                {this.renderSelectOptions(this.props.budgetList)}
                            </select>
                        </FormGroup>
                        <FormGroup label={'Cuisine?'}>
                            <select ref={'cuisine'} name="cuisine" id="cuisine-select" className="form-control"
                                    value={this.props.criteria.cuisine} onChange={this.updateCriteria.bind(this)}
                                    disabled={this.props.isLoading}>
                                {this.renderSelectOptions(this.props.cuisineList)}
                            </select>
                        </FormGroup>
                        <FormGroup label={'Atmosphere?'}>
                            <select ref={'atmosphere'} name="atmosphere" id="atmosphere-select" className="form-control"
                                    value={this.props.criteria.atmosphere} onChange={this.updateCriteria.bind(this)}
                                    disabled={this.props.isLoading}>
                                {this.renderSelectOptions(this.props.atmosphereList)}
                            </select>
                        </FormGroup>
                        <div className="col-lg-10 col-lg-offset-2">
                            <div className="col-lg-4">
                                <ToggleSwitch
                                    left={{
                            label: "Walking",
                            value: "walking",
                            onClick: e => this.updateTransportationMethod('walking')
                        }}
                                    right={{
                            label: "Driving",
                            value: "driving",
                            onClick:  e => this.updateTransportationMethod('driving')
                        }}
                                    name={'transportation-method'}
                                    selected={this.props.criteria.transportationMethod}
                                    disabled={this.props.isLoading}
                                />
                            </div>
                        </div>
                        <div className="col-lg-10 col-lg-offset-2">
                            <button className="btn btn-success" onClick={this.onClickRecommend.bind(this)}
                                    disabled={this.props.isLoading}>
                                Find Me Food!
                            </button>
                        </div>
                    </fieldset>
                </form>
            </div>
        )
    }
}