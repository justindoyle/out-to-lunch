import React from 'react'
import { Link } from 'react-router'


const RecommendationAccepted = () => {
    return (
        <div>
            <h1>Thank you for your feedback.</h1>
            <h3>Enjoy!</h3>
            <Link to="/" className="btn btn-primary">Home</Link>
        </div>
    )
};
export default RecommendationAccepted;