let instance = null;
import 'whatwg-fetch'

export default class OutToLunchApi
{
    constructor() {
        if (instance !== null) {
            return instance;
        }

        this.accessToken = null;
        this.tokenExpiresIn = null;
        this.tokenIssuedAt = null;
        this.refreshToken = null;
        this.baseUrl = '/api/';

        instance = this;
    }

    /**
     * Get a list of restaurant recommendations based on user input
     * @param lat
     * @param lng
     * @param urgency
     * @param budget
     * @param transportationMethod
     * @param cuisine
     * @param atmosphere
     * @returns {*}
     */
    recommendations(lat, lng, urgency, budget, transportationMethod, cuisine, atmosphere) {
        let query = this.buildQueryString({
            lat: lat,
            lng: lng,
            urgency: urgency,
            budget: budget,
            "transportation-method": transportationMethod,
            cuisine: cuisine,
            atmosphere: atmosphere
        });

        return this.get('recommendation?' + query);
    }

    /**
     * Accept a recommendation (tell OutToLunch it was a good recommendation)
     * @param id
     * @param responseToken
     * @returns {*}
     */
    acceptRecommendation(id, responseToken) {
        return this.post('recommendation/feedback', {
            'id': id,
            'response-token': responseToken,
            'was-good-recommendation': true
        });
    }

    buildQueryString(queryObject) {
        let queryString = '';
        for(let q in queryObject) {
            queryString += encodeURIComponent(q) + '=' + encodeURIComponent(queryObject[q]) + '&';
        }

        return queryString;
    }

    get(url) {
        return this.request(url, 'GET');
    }

    post(url, data) {
        return this.request(url, 'POST', data);
    }

    put(url, data) {
        return this.request(url, 'PUT', data);
    }

    request(url, method, data = {}) {
        let encodedUri = encodeURI(this.baseUrl + url);
        let fetchParams = {
            method: method,
            headers: this.requestHeaders(),
            mode: 'cors',
            credentials: 'same-origin'
        }

        if(method !== 'GET' && method !== 'HEAD') {
            fetchParams.body = JSON.stringify(data);
        }

        return new Promise((resolve, reject) => {

            fetch(encodedUri, fetchParams)
                .then(response => {
                    response.json()
                        .then(json => {
                            if (response.ok) {
                                resolve({response, json});
                            } else {
                                reject({response, json});
                            }
                        })
                        .catch(fail => {
                            console.log(response);
                            console.log(fail);
                            reject(fail);
                        })
                })
                .catch(fail => {
                    console.log("Request failed: " + encodedUri);
                    reject(fail);
                })
        });
    }

    requestHeaders() {
        let headers = {
            'Allow': 'HEAD,GET,POST,PUT,DELETE,PATCH',
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };
        if (this.accessToken !== null) {
            headers['Authorization'] = 'Bearer ' + this.accessToken;
        }

        return headers;
    }
}