<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Out To Lunch</title>

    <style>
        html, body {
            height: 100%
        }
    </style>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/toggle-switch.css')}}">

    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    @yield('headerInjection')
</head>
<body>


<div class="container">
    @yield('content')
</div>

<script type="application/javascript" src="{{asset('js/bundle.js')}}"></script>
@yield('footerInjection')
</body>
</html>