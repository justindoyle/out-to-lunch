var path = require('path');
var webpack = require('webpack');
var express = require('express');
var config = require('./webpack.config');
var cors = require('cors');

var app = express();
var compiler = webpack(config);

app.use(cors());
app.use(require('webpack-dev-middleware')(compiler, {
    publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

var corsOptions = {
    origin:'http://localhost:8888',
    optionsSuccessStatus: 200,
    credentials: true
}

app.get('*', cors(corsOptions), function(req, res) {
    res.sendFile(path.join(__dirname, 'public/index.php'));
});

app.listen(3000, function(err) {
    if (err) {
        return console.error(err);
    }

    console.log('Listening at http://localhost:3000/');
})